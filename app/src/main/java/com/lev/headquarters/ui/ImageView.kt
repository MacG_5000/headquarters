package com.lev.headquarters.ui


import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.lev.headquarters.R
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import jp.wasabeef.glide.transformations.SupportRSBlurTransformation

//import jp.wasabeef.glide.transformations.RoundedCornersTransformation
//import jp.wasabeef.glide.transformations.SupportRSBlurTransformation

fun ImageView.loadWithGlide(url: String?, @DrawableRes placeHolder: Int = R.color.squash) {
    Glide.with(context)
        .load(url)
        .apply(
            RequestOptions()
                .placeholder(placeHolder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(placeHolder)
                .dontTransform()
        )
        .into(this)

}

fun ImageView.loadRoundedCornersGlide(
    url: String?,
    radius: Int = 45, @DrawableRes placeHolder: Int = R.color.squash
) {
    val requestOptionRounded = RequestOptions.bitmapTransform(
        RoundedCornersTransformation(radius, 0, RoundedCornersTransformation.CornerType.ALL)
    )
    Glide.with(context)
        .load(url)
        .thumbnail(
            Glide.with(this)
                .load(placeHolder)
                .apply(requestOptionRounded)
        )
        .apply(requestOptionRounded)
        .into(this)
}

fun ImageView.loadCircleImageGlide(url: String?, @DrawableRes placeHolder: Int? = R.color.squash) {
    val rb = RequestOptions().circleCrop()
    if (placeHolder != null) {
        rb.placeholder(placeHolder).error(placeHolder).fallback(placeHolder).diskCacheStrategy
    }
    Glide.with(context)
        .load(url)
        .apply(rb)
        .into(this)
}

fun ImageView.loadBlurImage(url: String, sampling: Int) {
    Glide.with(context)
        .load(url)
        .apply(
            RequestOptions.bitmapTransform(
                SupportRSBlurTransformation(
                    20/*max 25 olmasi tavsiye edilir*/,
                    sampling
                )
            )
        )
        .into(this)
}