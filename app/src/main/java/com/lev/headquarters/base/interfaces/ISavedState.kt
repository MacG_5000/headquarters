package com.lev.headquarters.base.interfaces

import android.os.Bundle

interface ISavedState{
    fun onSaveInstanceState(outState: Bundle)
    fun onRestoreInstanceState(savedInstanceState: Bundle?)
}