package com.lev.headquarters.base.viewmodel

import androidx.lifecycle.ViewModel
import com.lev.headquarters.base.interfaces.IBaseSubscriber
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel(), IBaseSubscriber {
    protected val disposables: CompositeDisposable by lazy { CompositeDisposable() }

    override fun unSubscribe() {
        disposables.clear()
    }

    override fun subscribe(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribe()
    }
}