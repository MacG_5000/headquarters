package com.lev.headquarters.base.interfaces

import com.lev.headquarters.common.throwable.BaseThrowable

interface IBaseView {
    fun onError(t: BaseThrowable)
}