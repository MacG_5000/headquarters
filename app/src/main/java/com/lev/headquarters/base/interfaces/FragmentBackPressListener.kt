package com.lev.headquarters.base.interfaces

import android.view.KeyEvent
import androidx.fragment.app.Fragment

interface FragmentBackPressListener {

    fun Fragment.registerBackPressed(listener: () -> Unit) {
        view?.let {
            it.isFocusableInTouchMode = true
            it.requestFocus()
            it.setOnKeyListener { _, keyCode, event ->
                if (event.action != KeyEvent.ACTION_UP) return@setOnKeyListener true
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    listener.invoke()
                    return@setOnKeyListener true
                }
                false
            }
        }
    }
}