package com.lev.headquarters.base

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.lev.headquarters.base.interfaces.IBaseView
import com.lev.headquarters.base.interfaces.ILoadingIndicator
import com.lev.headquarters.common.LoadingIndicator
import com.lev.headquarters.common.Result
import com.lev.headquarters.common.throwable.BaseThrowable
import com.lev.headquarters.ktx.showInfoDialog
import com.lev.headquarters.ktx.showRetryDialog
import com.lev.headquarters.ktx.showToast
import com.lev.headquarters.widget.LoadingDialog
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

open class BaseFragment(@LayoutRes val contentLayoutId: Int) : Fragment(), IBaseView,
    ILoadingIndicator, HasSupportFragmentInjector {

    protected val loadingDialog: Dialog by lazy { LoadingDialog(requireContext()) }

    //@Inject lateinit var viewModelFactory: ViewModelFactory
    //@Inject lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(contentLayoutId, container, false)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = childFragmentInjector

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        //}
    }

    override fun onDestroyView() {
        //(activity as AppCompatActivity?)?.setSupportActionBar(null)
        super.onDestroyView()
    }

    override fun onError(t: BaseThrowable) {
        if (!isAdded) return
        setLoadingIndicator(t.loadingIndicator)

        if (t.finishScreen) {
            activity.showInfoDialog(t.uiMessage, positiveListener = { dialog ->
                dialog.dismiss()
                activity?.onBackPressed()
            })
            return
        }
        if (t.retryListener != null) {
            activity.showRetryDialog(t.uiMessage, positiveListener = t.retryListener,
                    negativeListener = { dialog ->
                        dialog.dismiss()
                        activity?.onBackPressed()
                    })
            return
        }
        if (t.showMessage) {
            activity?.showToast(t.uiMessage)
            //Can't use dialogs
            //activity.showInfoDialog(t.uiMessage)
        }
    }

    override fun setLoadingIndicator(loadingIndicator: LoadingIndicator) {
        if (!isAdded) return
        loadingDialog.apply {
            if (loadingIndicator.loadingVisibility) {
                if (!isShowing) show()
            } else
                if (isShowing) dismiss()
        }
        view?.isVisible = loadingIndicator.viewVisibility
    }

    protected fun <T> handleState(r: Result<T>): T? = when{
        r.isSuccess -> {
            setLoadingIndicator(r.loadingIndicator?: LoadingIndicator(false, true))
            r.data ?: run {
                onError(BaseThrowable())
                null
            }
        }
        r.isError -> {
            onError(r.error?: BaseThrowable())
            null
        }
        r.isLoading -> {
            setLoadingIndicator(r.loadingIndicator?: LoadingIndicator(true, true))
            null
        }
        else -> throw IllegalStateException("status must be succes, error or loading")
    }
}