package com.lev.headquarters.base.interfaces

import com.lev.headquarters.common.LoadingIndicator

interface ILoadingIndicator {
    fun setLoadingIndicator(loadingIndicator: LoadingIndicator)
}