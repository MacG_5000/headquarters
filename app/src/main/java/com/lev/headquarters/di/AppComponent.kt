package com.lev.headquarters.di

import com.lev.headquarters.application.AppControl
import com.lev.headquarters.di.module.AppModule
import com.lev.headquarters.di.module.ViewBuilderModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, ViewBuilderModule::class])
interface AppComponent : AndroidInjector<AppControl> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<AppControl>()
}

