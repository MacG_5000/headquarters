package com.lev.headquarters.di.module

import com.lev.headquarters.di.ActivityScope
import com.lev.headquarters.di.FragmentScope
import com.lev.headquarters.modules.detail.DetailFragment
import com.lev.headquarters.modules.main.MainActivity
import com.lev.headquarters.modules.tabs.nowPlaying.NowPlayingFragment
import com.lev.headquarters.modules.tabs.topRated.TopRatedFragment
import com.lev.headquarters.modules.tabs.upcoming.UpcomingFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ViewBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun bindTopRatedFragment(): TopRatedFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun bindUpcomingFragment(): UpcomingFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun bindNowPlayingFragment(): NowPlayingFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun bindDetailFragment(): DetailFragment

}