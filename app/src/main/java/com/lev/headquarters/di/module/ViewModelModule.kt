package com.lev.headquarters.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lev.headquarters.base.viewmodel.ViewModelFactory
import com.lev.headquarters.di.ViewModelKey
import com.lev.headquarters.modules.detail.DetailViewModel
import com.lev.headquarters.modules.tabs.nowPlaying.NowPlayingViewModel
import com.lev.headquarters.modules.tabs.topRated.TopRatedViewModel
import com.lev.headquarters.modules.tabs.upcoming.UpcomingViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(TopRatedViewModel::class)
    abstract fun bindTopRatedViewModel(viewModel: TopRatedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    abstract fun bindDetailViewModel(detailVM: DetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NowPlayingViewModel::class)
    abstract fun bindNowPlayingViewModel(detailVM: NowPlayingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UpcomingViewModel::class)
    abstract fun bindUpcomingViewModel(detailVM: UpcomingViewModel): ViewModel

}
