package com.lev.headquarters.di.module

import com.lev.headquarters.BuildConfig
import com.lev.headquarters.constants.ApiQuery
import com.lev.headquarters.constants.ApiUrls
import com.lev.headquarters.constants.Constants
import com.lev.headquarters.data.api.MovieService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
object NetworkModule {

    @Singleton
    @Provides
    @JvmStatic
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG) addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }
            .readTimeout(Constants.READ_TIME_OUT, TimeUnit.SECONDS)
            .connectTimeout(Constants.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val request = chain.request()
                val url = request.url().newBuilder().addQueryParameter(ApiQuery.API_KEY, Constants.API_KEY).build()
                chain.proceed(request.newBuilder().url(url).build())
            }
            .build()

    @Singleton
    @Provides
    @JvmStatic
    fun provideNetworkService(okHttpClient: OkHttpClient): MovieService =
        Retrofit.Builder().baseUrl(ApiUrls.ENDPOINT)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(MovieService::class.java)
}