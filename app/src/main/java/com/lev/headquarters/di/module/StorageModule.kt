package com.lev.headquarters.di.module

import android.content.Context
import android.content.SharedPreferences
import com.lev.headquarters.common.SecurePreferences
import com.lev.headquarters.constants.Config
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object StorageModule {

    @JvmStatic
    @Singleton
    @Provides
    fun providePreferences(context: Context): SharedPreferences = context.getSharedPreferences(
        Config.PREF_NAME, Context.MODE_PRIVATE)

    @JvmStatic
    @Singleton
    @Provides
    fun provideSecurePreferences(pref: SharedPreferences): SecurePreferences =
            SecurePreferences(pref, true, Config.E)

    /*@Singleton
    @JvmStatic
    @Provides
    fun provideSecurePreferencesManager(securePreferences: SecurePreferences): SecurePreferencesManager =
        SecurePreferencesManager(securePreferences)*/
}
