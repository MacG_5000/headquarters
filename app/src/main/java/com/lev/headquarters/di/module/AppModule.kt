package com.lev.headquarters.di.module

import android.content.Context
import com.lev.headquarters.application.AppControl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [StorageModule::class, ViewModelModule::class, NetworkModule::class])
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: AppControl): Context = application.applicationContext
}