package com.lev.headquarters.constants

object ApiUrls {
    const val ENDPOINT = "https://api.themoviedb.org/3/"

    const val SEARCH = "search/movie"
    const val MOVIE = "movie/{${ApiQuery.ID}}"

    const val UPCOMING = "movie/upcoming"
    const val TOP_RATED = "movie/top_rated"
    const val NOW_PLAYIN = "movie/now_playing"


}