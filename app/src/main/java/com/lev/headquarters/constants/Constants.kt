package com.lev.headquarters.constants

object Constants {
    const val API_KEY = "2696829a81b1b5827d515ff121700838"

    const val READ_TIME_OUT: Long = 60
    const val CONNECT_TIME_OUT: Long = 60

    private const val IMAGE_BASE_URL = "http://image.tmdb.org/t/p/"
    const val IMAGE_BASE_URL_LARGE = IMAGE_BASE_URL + "w500/"
    const val IMAGE_BASE_URL_SMALL = IMAGE_BASE_URL + "w92/"

    const val PAGE_SIZE = 20
    const val PREFETCH_DISTANCE = 20
}