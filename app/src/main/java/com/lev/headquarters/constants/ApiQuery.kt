package com.lev.headquarters.constants

object ApiQuery {
    const val API_KEY = "api_key"
    const val QUERY = "query"
    const val ID = "id"
    const val PAGE = "page"
}