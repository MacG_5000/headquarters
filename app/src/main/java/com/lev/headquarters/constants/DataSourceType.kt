package com.lev.headquarters.constants

object DataSourceType {

    const val UPCOMING = 1
    const val NOW_PLAYING = 2
    const val TOP_RATED = 3
}