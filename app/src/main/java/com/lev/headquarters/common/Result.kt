package com.lev.headquarters.common

import com.lev.headquarters.common.throwable.BaseThrowable

@Suppress("DataClassPrivateConstructor")
data class Result <out T> private constructor(val status: ViewStatus,
                                              val data: T?,
                                              val error: BaseThrowable?,
                                              val loadingIndicator: LoadingIndicator?) {
    companion object {
        fun <T> success(data: T, loadingIndicator: LoadingIndicator = LoadingIndicator(false, true)): Result<T> =
                Result(ViewStatus.SUCCESS, data, null, loadingIndicator)

        fun <T> error(error: BaseThrowable): Result<T> =
                Result(ViewStatus.ERROR, null, error, null)

        fun <T> loading(loadingIndicator: LoadingIndicator = LoadingIndicator(true, true)): Result<T> =
                Result(ViewStatus.LOADING, null, null, loadingIndicator)
    }

    val isSuccess
        get() = ViewStatus.SUCCESS == status

    val isError
        get() = ViewStatus.ERROR == status

    val isLoading
        get() = ViewStatus.LOADING == status

}