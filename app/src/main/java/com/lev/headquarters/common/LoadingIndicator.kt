package com.lev.headquarters.common

data class LoadingIndicator(val loadingVisibility: Boolean,
                            val viewVisibility: Boolean)