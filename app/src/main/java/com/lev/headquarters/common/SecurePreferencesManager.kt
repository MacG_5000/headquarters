package com.lev.headquarters.common

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SecurePreferencesManager @Inject constructor(private val securePreferences: SecurePreferences){

    companion object {
        private const val KEY_IDS = "ids"
    }

    private val gson = Gson()

    private fun insert(key: String, value: String) {
        securePreferences.put(key, value)
    }

    fun getString(key: String) = securePreferences.getString(key)

    var favoritesIds: ArrayList<Int>
        get() =  gson.fromJson<ArrayList<Int>>(getString(KEY_IDS), object : TypeToken<ArrayList<Int>>() { }.type) ?: ArrayList()
        set(value) = insert(KEY_IDS, gson.toJson(value))

    fun isFavorite(id: Int): Boolean{
        //gson.fromJson("asd", object : TypeToken<ArrayList<String>>() {}.type)
        return favoritesIds.contains(id)
    }

    fun addToFavorites(id: Int){
        var list = favoritesIds
        if(!list.contains(id)){
            list.add(id)
            favoritesIds = list
        }
    }

    fun removeFromFavorites(id: Int){
        var list: ArrayList<Int>? = favoritesIds
        if(list!!.contains(id)){
            list.remove(id)
            favoritesIds = list
        }
    }

    /*var lastSearchBundle: SearchBundle? = null
        get() {
            if (field == null)
                field = searchBundleList?.firstOrNull { it.calendarData?.isCachable == true }
            return field
        }
        private set

    private val searchBundleList: MutableList<SearchBundle>?
        get() = gson.fromJson(getString(KEY_SEARCH_CACHE), object : TypeToken<ArrayList<SearchBundle>?>() {}.type)

    val safeSearchBundleList: MutableList<SearchBundle>?
        get() = searchBundleList?.filter { it.calendarData?.isCachable == true }?.toMutableList()*/
}