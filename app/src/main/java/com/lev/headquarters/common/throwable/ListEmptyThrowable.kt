package com.lev.headquarters.common.throwable

import com.lev.headquarters.R

class ListEmptyThrowable(uiMessage: Any = ""): BaseThrowable(uiMessage){

    override var noneEmptyMessage = R.string.error_empty
}