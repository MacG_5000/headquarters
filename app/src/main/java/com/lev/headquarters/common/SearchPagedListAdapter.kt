package com.lev.headquarters.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.lev.headquarters.R
import com.lev.headquarters.constants.Constants
import com.lev.headquarters.data.api.entity.Movie
import com.lev.headquarters.ui.loadWithGlide
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.dialog_generic.tvTitle
import kotlinx.android.synthetic.main.list_item_movie.*
import javax.inject.Inject

class SearchPagedListAdapter @Inject constructor() :
    PagedListAdapter<Movie, SearchPagedListAdapter.MovieViewHolder>(diffCallback) {

    var listener: ((Movie, ImageView) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder =
        MovieViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_movie, parent, false), listener)

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) = holder.bind(getItem(position))

    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie) = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie) = oldItem == newItem
        }
    }

    class MovieViewHolder(override val containerView: View, private val listener: ((Movie, ImageView) -> Unit)?) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(movie: Movie?) {
            movie?.let {
                //ivPoster.transitionName = movie.id.toString()
                containerView.setOnClickListener { listener?.invoke(movie, ivPoster) }
                tvTitle.text = movie.title
                tvOverview.text = movie.overview
                tvScore.isVisible = movie.voteAverage != 0.0
                tvScore.text = movie.voteAverage.toString()
                tvReleaseDate.text = movie.releaseDate
                ivPoster.loadWithGlide(Constants.IMAGE_BASE_URL_SMALL + movie.posterPath)
            }
        }
    }
}