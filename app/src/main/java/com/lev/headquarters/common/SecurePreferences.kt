package com.lev.headquarters.common

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.util.Base64
import java.io.UnsupportedEncodingException
import java.security.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * This will initialize an instance of the SecurePreferences class
 * @param context your current context.
 * @param preferenceName name of preferences file (preferenceName.xml)
 * @param secureKey the key used for encryption, finding a good key scheme is hard.
 * Hardcoding your key in the application is bad, but better than plaintext preferences. Having the user enter the key upon application launch is a safe(r) alternative, but annoying to the user.
 * @param encryptKeys settings this to false will only encrypt the values,
 * true will encrypt both values and keys. Keys can contain a lot of information about
 * the plaintext value of the value which can be used to decipher the value.
 * @throws SecurePreferencesException
 */

class SecurePreferencesException(e: Throwable) : RuntimeException(e)

class SecurePreferences @Throws(SecurePreferencesException::class) constructor(private val preferences: SharedPreferences,
                                                                               private val encryptKeys: Boolean,
                                                                               secureKey: String) {
    companion object {
        private const val TRANSFORMATION = "AES/CBC/PKCS5Padding"
        private const val KEY_TRANSFORMATION = "AES/ECB/PKCS5Padding"
        private const val SECRET_KEY_HASH_TRANSFORMATION = "SHA-256"
        private const val CHARSET = "UTF-8"

        @Throws(SecurePreferencesException::class)
        private fun convert(cipher: Cipher, bs: ByteArray): ByteArray {
            try {
                return cipher.doFinal(bs)
            } catch (e: Exception) {
                throw SecurePreferencesException(e)
            }

        }
    }
    private val writer: Cipher
    private val reader: Cipher
    private val keyWriter: Cipher

    private val iv: IvParameterSpec
        get() {
            val iv = ByteArray(writer.blockSize)
            System.arraycopy("herseycokguzelolacak".toByteArray(), 0, iv, 0, writer.blockSize)
            return IvParameterSpec(iv)
        }

    init {
        try {
            this.writer = Cipher.getInstance(TRANSFORMATION)
            this.reader = Cipher.getInstance(TRANSFORMATION)
            this.keyWriter = Cipher.getInstance(KEY_TRANSFORMATION)
            initCiphers(secureKey)
        } catch (e: GeneralSecurityException) {
            throw SecurePreferencesException(e)
        } catch (e: UnsupportedEncodingException) {
            throw SecurePreferencesException(e)
        }

    }

    @Throws(UnsupportedEncodingException::class, NoSuchAlgorithmException::class, InvalidKeyException::class, InvalidAlgorithmParameterException::class)
    private fun initCiphers(secureKey: String) {
        val ivSpec = iv
        val secretKey = getSecretKey(secureKey)
        writer.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec)
        reader.init(Cipher.DECRYPT_MODE, secretKey, ivSpec)
        keyWriter.init(Cipher.ENCRYPT_MODE, secretKey)
    }

    @Throws(UnsupportedEncodingException::class, NoSuchAlgorithmException::class)
    private fun getSecretKey(key: String): SecretKeySpec {
        val keyBytes = createKeyBytes(key)
        return SecretKeySpec(keyBytes, TRANSFORMATION)
    }

    @Throws(UnsupportedEncodingException::class, NoSuchAlgorithmException::class)
    private fun createKeyBytes(key: String): ByteArray {
        val md = MessageDigest.getInstance(SECRET_KEY_HASH_TRANSFORMATION)
        md.reset()
        return md.digest(key.toByteArray(charset(CHARSET)))
    }


    @Throws(SecurePreferencesException::class)
    fun put(key: String, value: String?) {
        if (value == null) {
            preferences.edit().remove(toKey(key)).apply()
        } else {
            val secureValueEncoded = encrypt(value, writer)
            preferences.edit().putString(toKey(key), secureValueEncoded).apply()
        }
    }

    @SuppressLint("ApplySharedPref")
    @Throws(SecurePreferencesException::class)
    fun setValue(key: String, value: String?) {
        if (value == null) {
            preferences.edit().remove(toKey(key)).commit()
        } else {
            val secureValueEncoded = encrypt(value, writer)
            preferences.edit().putString(key, secureValueEncoded).commit()
        }
    }

    fun containsKey(key: String): Boolean {
        return preferences.contains(toKey(key))
    }

    fun removeValue(key: String) {
        preferences.edit().remove(toKey(key)).apply()
    }

    @Throws(SecurePreferencesException::class)
    fun getString(key: String): String? {
        if (preferences.contains(toKey(key))) {
            val securedEncodedValue = preferences.getString(toKey(key), "") ?: ""
            return decrypt(securedEncodedValue)
        }
        return null
    }

    fun clear() {
        preferences.edit().clear().apply()
    }

    private fun toKey(key: String): String {
        return if (encryptKeys)
            encrypt(key, keyWriter)
        else
            key
    }

    @Throws(SecurePreferencesException::class)
    private fun encrypt(value: String, writer: Cipher): String {
        val secureValue: ByteArray
        try {
            secureValue = convert(writer, value.toByteArray(charset(CHARSET)))
        } catch (e: UnsupportedEncodingException) {
            throw SecurePreferencesException(e)
        }
        return Base64.encodeToString(secureValue, Base64.NO_WRAP)
    }

    private fun decrypt(securedEncodedValue: String): String {
        val securedValue = Base64.decode(securedEncodedValue, Base64.NO_WRAP)
        val value = convert(reader, securedValue)
        try {
            return String(value, charset(CHARSET))
        } catch (e: UnsupportedEncodingException) {
            throw SecurePreferencesException(e)
        }
    }
}