package com.lev.headquarters.common

enum class ViewStatus { SUCCESS, ERROR, LOADING }
