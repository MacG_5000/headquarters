package com.lev.headquarters.common.throwable

import com.lev.headquarters.R
import com.lev.headquarters.common.LoadingIndicator
import com.lev.headquarters.widget.dialogs.base.IDialogInterface

open class BaseThrowable: Throwable{

    var showMessage: Boolean = true
        private set
    var loadingIndicator: LoadingIndicator =
        LoadingIndicator(loadingVisibility = false, viewVisibility = true)
        private set
    var finishScreen: Boolean = false
        private set
    var uiMessage: Any
        get() = if (-1 == field || "" == field) noneEmptyMessage else field
        private set

    var retryListener: ((dialog: IDialogInterface) -> Unit)? = null
        private set


    fun setUiMessageForAccess(message: Any){
        uiMessage = message
    }

    protected open var noneEmptyMessage = R.string.error_generic

    constructor(cause: Throwable,
                uiMessage: Any = "",
                finishScreen: Boolean = false,
                showMessage:Boolean = true,
                loadingIndicator: LoadingIndicator = LoadingIndicator(
                    loadingVisibility = false,
                    viewVisibility = true
                ),
                retryListener: ((dialog: IDialogInterface) -> Unit)? = null) : super(cause) {
        this.showMessage = showMessage
        this.loadingIndicator = loadingIndicator
        this.finishScreen = finishScreen
        this.uiMessage = uiMessage
        this.retryListener = retryListener
    }

    constructor(uiMessage: Any = "",
                finishScreen: Boolean = false,
                showMessage:Boolean = true,
                loadingIndicator: LoadingIndicator = LoadingIndicator(
                    loadingVisibility = false,
                    viewVisibility = true
                ),
                retryListener: ((dialog: IDialogInterface) -> Unit)? = null) : super(uiMessage.toString()) {
        this.showMessage = showMessage
        this.loadingIndicator = loadingIndicator
        this.finishScreen = finishScreen
        this.uiMessage = uiMessage
        this.retryListener = retryListener
    }

}