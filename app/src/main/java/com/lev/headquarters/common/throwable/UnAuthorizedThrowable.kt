package com.lev.headquarters.common.throwable

import com.lev.headquarters.R


class UnAuthorizedThrowable (uiMessage: String = ""): BaseThrowable(uiMessage, finishScreen = true){

    override var noneEmptyMessage = R.string.error_user_unauthorized
}