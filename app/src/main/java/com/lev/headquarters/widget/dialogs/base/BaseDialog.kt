package com.lev.headquarters.widget.dialogs.base

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.ViewGroup
import com.lev.headquarters.R

open class BaseDialog(context: Context, theme: Int) : Dialog(context, theme), IDialogInterface {

    init {
        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.setWindowAnimations(R.style.Widget_Lib_GenericDialog_Animation)
    }
}