package com.lev.headquarters.widget.dialogs.base

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes

open class BaseDialogParam(val title: String?,
                           val positiveButtonText: String?,
                           val negativeButtonText: String?,
                           val neutralButtonText: String?,
                           val cancelable: Boolean,
                           val onBackPressedCancelable: Boolean,
                           @DrawableRes val imageResourceId: Int?,
                           @ColorRes val titleColorResId: Int?,
                           @ColorRes val positiveButtonTextColorResId: Int?,
                           @ColorRes val negativeButtonTextColorResId: Int?,
                           @ColorRes val neutralButtonTextColorResId: Int?,
                           val dismissListener: ((IDialogInterface) -> Unit)?,
                           val positiveButtonClickListener: ((IDialogInterface) -> Unit)?,
                           val negativeButtonClickListener: ((IDialogInterface) -> Unit)?,
                           val neutralButtonClickListener: ((IDialogInterface) -> Unit)?)
