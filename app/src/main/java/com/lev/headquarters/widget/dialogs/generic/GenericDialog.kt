package com.lev.headquarters.widget.dialogs.generic

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.transition.Slide
import com.lev.headquarters.R
import com.lev.headquarters.ktx.asString
import com.lev.headquarters.ktx.color
import com.lev.headquarters.ktx.isOverApi
import com.lev.headquarters.widget.dialogs.base.BaseDialog
import com.lev.headquarters.widget.dialogs.base.IDialogInterface
import kotlinx.android.synthetic.main.dialog_generic.*

class GenericDialog private constructor(context: Context, theme: Int) : BaseDialog(context, theme),
    IDialogInterface {

    lateinit var p: GenericDialogParam
    private var dismissListener: ((IDialogInterface) -> Unit)? = null
    private var onBackPressedCancelable: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_generic)
        //if (isUnderApi(Build.VERSION_CODES.LOLLIPOP)) {
            //apply(p)
        //}
    }

    private fun apply(p: GenericDialogParam) {
        /** message */
        p.message?.let { message ->
            tvMessage.apply {
                isVisible = true
                text = message //if (p.makeHtml) message.makeHtml() else
                p.messageColorResId?.let { setTextColor(context.color(it)) }
                p.messageGravity?.let { gravity = it }
            }
        } ?: kotlin.run { tvMessage.isVisible = false }
        /** title **/
        p.title?.let { title ->
            llTopPanel.isVisible = true
            noTitleSpace.isVisible = false
            tvTitle.apply {
                isVisible = true
                text = title
                p.titleColorResId?.let { setTextColor(context.color(it)) }
            }
        } ?: kotlin.run {
            llTopPanel.isVisible = false
            noTitleSpace.isVisible = true
        }
        /** subTitle **/
        p.subTitle?.let { subTitle ->
            tvSubTitle.apply {
                isVisible = true
                text = subTitle
                p.subTitleColorResId?.let { setTextColor(context.color(it)) }
            }
        } ?: kotlin.run { tvSubTitle.isVisible = false }
        /** image */
        p.imageResourceId?.let {
            ivAlert.apply {
                isVisible = true
                setImageResource(it)
            }
        } ?: kotlin.run { ivAlert.isVisible = false }

        /** positive button is always visible **/
        p.positiveButtonText?.let { btnPositive.text = it }
            ?: kotlin.run { btnPositive.text = context.resources.getString(R.string.ok) }
        p.positiveButtonTextColorResId?.let { btnPositive.setTextColor(context.color(it)) }
        p.positiveButtonClickListener?.let { t -> btnPositive.setOnClickListener { t(this) } }
            ?: btnPositive.setOnClickListener { dismiss() }

        /** negative button */
        if (p.negativeButtonText != null || p.negativeButtonClickListener != null) {
            btnNegative.isVisible = true
            p.negativeButtonText?.let { btnNegative.text = it }
                ?: kotlin.run { btnNegative.text = context.resources.getString(R.string.cancel) }
            p.negativeButtonTextColorResId?.let { btnNegative.setTextColor(context.color(it)) }
            p.negativeButtonClickListener?.let { t -> btnNegative.setOnClickListener { t(this) } }
                ?: btnNegative.setOnClickListener { dismiss() }
        } else {
            btnNegative.isVisible = false
        }

        /** neutral button */ //clicklistener and text both them must be implement
        if (p.neutralButtonText != null && p.neutralButtonClickListener != null) {
            btnNeutral.apply {
                btnNeutral.isVisible = true
                text = p.neutralButtonText
                setOnClickListener { p.neutralButtonClickListener.invoke(this@GenericDialog) }
                p.neutralButtonTextColorResId?.let { setTextColor(context.color(it)) }
            }
        } else {
            btnNeutral.isVisible = false
        }

        setCancelable(p.cancelable)
        dismissListener = p.dismissListener
        // top and bottom indicators setting
        val indicators =
            (if (llTopPanel.isVisible) ViewCompat.SCROLL_INDICATOR_TOP else 0) or (if (llButtonPanel.isVisible) ViewCompat.SCROLL_INDICATOR_BOTTOM else 0)
        setScrollIndicators(
            contentPanel,
            scrollView,
            indicators,
            ViewCompat.SCROLL_INDICATOR_TOP or ViewCompat.SCROLL_INDICATOR_BOTTOM
        )
    }

    override fun dismiss() {
        dismissListener?.invoke(this)
        super.dismiss()
    }

    override fun onBackPressed() {
        if (onBackPressedCancelable)
            super.onBackPressed()
    }

    private fun setScrollIndicators(contentPanel: ViewGroup, content: View, indicators: Int, mask: Int) {
        if (isOverApi(23)) {
            // We're on Marshmallow so can rely on the View APIs
            ViewCompat.setScrollIndicators(content, indicators, mask)
            // We can also remove the compat indicator views
            if (scrollIndicatorUp != null) {
                contentPanel.removeView(scrollIndicatorUp)
            }
            if (scrollIndicatorDown != null) {
                contentPanel.removeView(scrollIndicatorDown)
            }
        } else {
            var indicatorUp: View? = scrollIndicatorUp
            var indicatorDown: View? = scrollIndicatorDown

            if (scrollIndicatorUp != null && indicators and ViewCompat.SCROLL_INDICATOR_TOP == 0) {
                contentPanel.removeView(scrollIndicatorUp)
                indicatorUp = null
            }
            if (scrollIndicatorUp != null && indicators and ViewCompat.SCROLL_INDICATOR_BOTTOM == 0) {
                contentPanel.removeView(scrollIndicatorUp)
                indicatorDown = null
            }
            scrollView.setOnScrollChangeListener(
                NestedScrollView.OnScrollChangeListener { v, _, _, _, _ ->
                    manageScrollIndicators(v, indicatorUp, indicatorDown)
                })
            // Set up the indicators following layout.
            scrollView.post { manageScrollIndicators(scrollView, indicatorUp, indicatorDown) }
        }

    }

    private fun manageScrollIndicators(v: View, upIndicator: View?, downIndicator: View?) {
        if (upIndicator != null) {
            upIndicator.visibility = if (v.canScrollVertically(-1)) View.VISIBLE else View.INVISIBLE
        }
        if (downIndicator != null) {
            downIndicator.visibility = if (v.canScrollVertically(1)) View.VISIBLE else View.INVISIBLE
        }
    }

    class Builder(val context: Context) {
        var makeHtml: Boolean = false
            private set
        var title: String? = null
            private set
        var subTitle: String? = null
            private set
        var message: String? = null
            private set
        var positiveButtonText: String? = null
            private set
        var negativeButtonText: String? = null
            private set
        var neutralButtonText: String? = null
            private set
        var cancelable: Boolean = false
            private set
        var onBackPressedCancelable: Boolean = false
            private set
        @DrawableRes
        var imageResourceId: Int? = null
            private set
        @ColorRes
        var titleColorResId: Int? = null
            private set
        @ColorRes
        var subTitleColorResId: Int? = null
            private set
        @ColorRes
        var messageColorResId: Int? = null
            private set
        @ColorRes
        var positiveButtonTextColorResId: Int? = null
            private set
        @ColorRes
        var negativeButtonTextColorResId: Int? = null
            private set
        @ColorRes
        var neutralButtonTextColorResId: Int? = null
            private set
        @Slide.GravityFlag
        var messageGravity: Int = Gravity.LEFT
            private set
        var dismissListener: ((IDialogInterface) -> Unit)? = null
            private set
        var positiveButtonClickListener: ((IDialogInterface) -> Unit)? = null
            private set
        var negativeButtonClickListener: ((IDialogInterface) -> Unit)? = null
            private set
        var neutralButtonClickListener: ((IDialogInterface) -> Unit)? = null
            private set

        fun setMessage(text: Any, makeHtml: Boolean = false, gravity: Int = Gravity.START, @ColorRes messageColorResId: Int? = null): Builder = this.apply {
            this.message = context.asString(text)
            this.makeHtml = makeHtml
            this.messageGravity = gravity
            this.messageColorResId = messageColorResId
        }

        fun setTitle(text: Any?, @ColorRes titleColorResId: Int? = null): Builder = this.apply {
            this.title = context.asString(text)
            this.titleColorResId = titleColorResId

        }

        fun setSubTitle(subTitle: Any?, @ColorRes subTitleColorResId: Int? = null) = this.apply {
            this.subTitle = context.asString(subTitle)
            this.subTitleColorResId = subTitleColorResId
        }

        fun setImage(@DrawableRes imageResId: Int?) = this.apply {
            this.imageResourceId = imageResId
        }

        fun setPositiveButton(
            text: Any? = null, @ColorRes textColorResId: Int? = null,
            positiveListener: ((dialogPos: IDialogInterface) -> Unit)? = null
        ) = this.apply {
            this.positiveButtonText = context.asString(text)
            this.positiveButtonTextColorResId = textColorResId
            this.positiveButtonClickListener = positiveListener
        }

        fun setNegativeButton(
            text: Any? = null, @ColorRes textColorResId: Int? = null,
            negativeListener: ((dialogNeg: IDialogInterface) -> Unit)? = null
        ) = this.apply {
            this.negativeButtonText = context.asString(text)
            this.negativeButtonTextColorResId = textColorResId
            this.negativeButtonClickListener = negativeListener
        }

        fun setNeutralButton(
            text: Any? = null, @ColorRes textColorResId: Int? = null,
            neutralListener: ((dialogNeut: IDialogInterface) -> Unit)? = null
        ) = this.apply {
            this.neutralButtonText = context.asString(text)
            this.neutralButtonTextColorResId = textColorResId
            this.neutralButtonClickListener = neutralListener
        }

        fun setOnBackPressedCancelable(backCancelable: Boolean) = this.apply {
            this.onBackPressedCancelable = backCancelable
        }

        fun setCancelable(cancelable: Boolean) = this.apply {
            this.cancelable = cancelable
        }

        fun createDialog(): GenericDialog = GenericDialog(context, R.style.Widget_Lib_GenericDialog).apply {
            if (isOverApi(21)){
                //create()
                apply(GenericDialogParam(this@Builder))
            }
            else
                p = GenericDialogParam(this@Builder)
        }


        fun show() = createDialog().apply { show() }
    }
}