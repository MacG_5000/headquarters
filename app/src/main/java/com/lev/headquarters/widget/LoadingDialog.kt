package com.lev.headquarters.widget

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.ViewGroup
import com.lev.headquarters.R

class LoadingDialog(context: Context) : Dialog(context) {
    var isActive: Boolean = false
        private set

    init {
        if (context is Activity) {
            ownerActivity = context
        }
        window?.let {
            it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            it.setWindowAnimations(R.style.Widget_Lib_LoadingDialogAnimation)
        }
        setCanceledOnTouchOutside(false)
        setCancelable(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_loading)
    }

    override fun onBackPressed() {
        dismiss()
        ownerActivity?.onBackPressed()
        super.onBackPressed()
    }

    override fun dismiss() {
        isActive = false
        super.dismiss()
    }

    override fun show() {
        super.show()
        isActive = true
    }

}