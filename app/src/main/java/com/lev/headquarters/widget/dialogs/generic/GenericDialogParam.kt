package com.lev.headquarters.widget.dialogs.generic

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.transition.Slide
import com.lev.headquarters.widget.dialogs.base.BaseDialogParam
import com.lev.headquarters.widget.dialogs.base.IDialogInterface

class GenericDialogParam private constructor(title: String?,
                                             val subTitle: String?,
                                             val message: String?,
                                             positiveButtonText: String?,
                                             negativeButtonText: String?,
                                             neutralButtonText: String?,
                                             cancelable: Boolean,
                                             onBackPressedCancelable: Boolean,
                                             @DrawableRes imageResourceId: Int?,
                                             @ColorRes titleColorResId: Int?,
                                             @ColorRes val subTitleColorResId: Int?,
                                             @ColorRes val messageColorResId: Int?,
                                             @ColorRes positiveButtonTextColorResId: Int?,
                                             @ColorRes negativeButtonTextColorResId: Int?,
                                             @ColorRes neutralButtonTextColorResId: Int?,
                                             @Slide.GravityFlag val messageGravity: Int?,
                                             dismissListener: ((IDialogInterface) -> Unit)?,
                                             positiveButtonClickListener: ((IDialogInterface) -> Unit)?,
                                             negativeButtonClickListener: ((IDialogInterface) -> Unit)?,
                                             neutralButtonClickListener: ((IDialogInterface) -> Unit)?,
                                             val makeHtml: Boolean)
    : BaseDialogParam(title, positiveButtonText, negativeButtonText, neutralButtonText, cancelable, onBackPressedCancelable, imageResourceId,
        titleColorResId, positiveButtonTextColorResId, negativeButtonTextColorResId, neutralButtonTextColorResId,
        dismissListener, positiveButtonClickListener, negativeButtonClickListener, neutralButtonClickListener) {
    constructor(b: GenericDialog.Builder) : this(b.title, b.subTitle, b.message, b.positiveButtonText, b.negativeButtonText, b.neutralButtonText, b.cancelable,
            b.onBackPressedCancelable, b.imageResourceId, b.titleColorResId, b.subTitleColorResId, b.messageColorResId, b.positiveButtonTextColorResId, b.negativeButtonTextColorResId,
            b.neutralButtonTextColorResId, b.messageGravity, b.dismissListener, b.positiveButtonClickListener,
            b.negativeButtonClickListener, b.neutralButtonClickListener, b.makeHtml)
}