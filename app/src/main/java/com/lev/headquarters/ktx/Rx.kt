package com.lev.headquarters.ktx

import androidx.lifecycle.MutableLiveData
import com.lev.headquarters.common.Result
import com.lev.headquarters.common.throwable.BaseThrowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.annotations.CheckReturnValue
import io.reactivex.annotations.SchedulerSupport
import io.reactivex.disposables.Disposable

@CheckReturnValue
@SchedulerSupport(SchedulerSupport.NONE)
fun <T, R : Result<T>> Observable<R>.handle(liveData: MutableLiveData<R>,
                                            throwable: BaseThrowable = BaseThrowable()
): Disposable {
    return subscribe(
            { t: R -> liveData.value = t },
            { e: Throwable ->
                liveData.value = Result.error<T>(throwable) as R
            }
    )
}

@CheckReturnValue
@SchedulerSupport(SchedulerSupport.NONE)
fun <T, R : Result<T>> Observable<R>.handle(onNext: (T) -> Unit, liveData: MutableLiveData<Result<T>>,
                                            throwable: BaseThrowable = BaseThrowable()
): Disposable {
    return subscribe(
            { t: R -> if (t.isSuccess)
                onNext.invoke(t.data!!)
            else
                liveData.value = t },
            { e: Throwable ->
                if (e is BaseThrowable) {
                    liveData.value = Result.error(e)
                } else {
                    liveData.value = Result.error(throwable)
                }
            }
    )
}


fun <T> Single<T>.toResult(): Observable<Result<T>> {
    return toObservable()
            .map { Result.success(it) }
            .startWith(Result.loading())
            .onErrorReturn { t: Throwable ->
                when (t) {
                    is BaseThrowable -> Result.error(t)
                    else -> Result.error(BaseThrowable(t))
                }
            }
}