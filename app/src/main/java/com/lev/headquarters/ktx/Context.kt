package com.lev.headquarters.ktx

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.lev.headquarters.R

@Suppress("DEPRECATION")
fun Context.color(@ColorRes colorResId: Int, theme: Resources.Theme? = null): Int =
    resources.getColor(colorResId)
        //if (isOverApi(Build.VERSION_CODES.M))
            //resources.getColor(colorResId, theme) else resources.getColor(colorResId)

@Suppress("DEPRECATION")
fun Context.drawable(colorResId: Int, theme: Resources.Theme? = null): Drawable =
    resources.getDrawable(colorResId)
        //(if (isOverApi(Build.VERSION_CODES.LOLLIPOP))
        //    resources.getDrawable(colorResId, theme) else resources.getDrawable(colorResId))

fun Context.asString(p0: Any?): String? = when (p0) {
    is String -> p0
    is @StringRes Int -> resources.getString(p0)
    else -> null
}

fun Context.asStringOrEmpty(p0: Any?): String = when (p0) {
    is String -> p0
    is @StringRes Int -> resources.getString(p0)
    else -> ""
}

fun Context.inflater(): LayoutInflater = if (this is Activity) this.layoutInflater else LayoutInflater.from(this)

fun Context.showToast(message: Any, duration: Int = Toast.LENGTH_LONG) {
    asString(message)?.let {
        Toast.makeText(this, it, duration).show()
        /*val t = Toast.makeText(this, it, duration)
        val tv = t.view.findViewById<TextView?>(android.R.id.message)
        if (tv != null)
            tv.gravity = Gravity.CENTER
        t.show()*/
    }
}

fun Context.share(text: String, subject: String = ""): Boolean {
    val intent = Intent(Intent.ACTION_SEND)
    intent.type = "text/plain"
    intent.putExtra(Intent.EXTRA_SUBJECT, subject)
    intent.putExtra(Intent.EXTRA_TEXT, text)
    return try {
        startActivity(Intent.createChooser(intent, null))
        true
    } catch (e: ActivityNotFoundException) {
        false
    }
}

fun Context.copyToClipboard(text: Any) {
    val sText = this.asString(text)
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
    val clip = android.content.ClipData.newPlainText("Copied Text", sText)
    clipboard.primaryClip = clip
    showToast((this.resources.getString(R.string.copied_clipboard) + sText), Toast.LENGTH_SHORT)
}