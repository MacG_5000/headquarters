package com.lev.headquarters.ktx

import android.os.Build

internal fun isOverApi(minimumSdk: Int): Boolean {
    return Build.VERSION.SDK_INT >= minimumSdk
}

internal fun isUnderApi(maximumSdk: Int): Boolean {
    return Build.VERSION.SDK_INT <= maximumSdk
}