@file:Suppress("NOTHING_TO_INLINE")

package com.lev.headquarters.ktx

import org.joda.time.Days
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import java.util.*

const val DATE_FORMAT1 = "yyyyMMdd"
const val DATE_FORMAT2 = "yyyy-MM-dd"
const val DATE_FORMAT3 = "d MMM EE"
const val DATE_FORMAT4 = "dd.MM.yyyy"
const val DATE_FORMAT5 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
const val DATE_FORMAT16 = "HH:mm"
const val DATE_FORMAT8 = "d MMM"
const val DATE_FORMAT9 = "d MMMM"
const val DATE_FORMAT10 = "d MMMM yyyy"
const val DATE_FORMAT12 = "dd/MM/yyyy"
const val DATE_FORMAT13 = "d MMMM yyyy, EE"
const val DATE_FORMAT14 = "dd MMM yyyy"
const val DATE_FORMAT15 = "dd MMMM yyyy"
const val DATE_FORMAT17 = "yyyy-MM-dd"
const val DATE_FORMAT18 = "MMMM yyyy"
val formats = arrayOf(
        DATE_FORMAT1,
        DATE_FORMAT2,
        DATE_FORMAT3,
        DATE_FORMAT4,
        DATE_FORMAT5,
        DATE_FORMAT8,
        DATE_FORMAT9,
        DATE_FORMAT10,
        DATE_FORMAT12,
        DATE_FORMAT13,
        DATE_FORMAT14,
        DATE_FORMAT15
)

inline fun Date.toLocalDateTime(): LocalDateTime = LocalDateTime(this)

inline fun LocalDateTime.asString(targetFormat: String): String = toString(targetFormat)

inline fun LocalDateTime.differenceDay(recentlyDate: String, pastDate: String): Int {
    val rDate = recentlyDate.parseLocalDate
    val pDate = pastDate.parseLocalDate
    return if (rDate != null && pDate != null) Math.abs(Days.daysBetween(rDate, pDate).days) else -1
}

inline val LocalDateTime.differenceDayFromNow: Int
    get() = Days.daysBetween(LocalDate(), this).days

inline val LocalDateTime.isPast: Boolean
    get() = differenceDayFromNow < 0

inline val LocalDateTime.isPastMoreOneDay: Boolean
    get() = differenceDayFromNow < -1

inline val LocalDateTime.isToday: Boolean
    get() = isEqual(LocalDate())

inline val LocalDateTime.isTomorrow: Boolean
    get() = differenceDayFromNow == 1

inline fun LocalDateTime.getDayName(): String {
    return dayOfWeek().getAsText(Locale.getDefault())

}

/**---------------Strings-----------------------------*/

inline val String.parseLocalDate: LocalDateTime?
    get() {
        for (f in formats) {
            val dtf = DateTimeFormat.forPattern(f)
            try {
                return dtf.parseDateTime(this).toLocalDateTime()
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }
        }
        //Crashlytics.logException(Throwable("Date cannot parsed, no matched format"))
        return null
    }

inline fun String.parseLocalDate(sourceFormat: String): LocalDateTime? {
    val dtf = DateTimeFormat.forPattern(sourceFormat)
    try {
        return dtf.parseDateTime(this).toLocalDateTime()
    } catch (e: IllegalArgumentException) {
        e.printStackTrace()
        //Crashlytics.logException(e)
    }
    return null
}

inline fun String.changeDateFormat(sourceFormat: String, targetFormat: String): String? = parseLocalDate(sourceFormat)?.asString(targetFormat)