package com.lev.headquarters.ktx

import android.app.Activity
import android.view.Gravity
import com.lev.headquarters.R
import com.lev.headquarters.widget.dialogs.base.IDialogInterface
import com.lev.headquarters.widget.dialogs.generic.GenericDialog


/** Info Dialog *
 * @param message : @nonnull message
 * @param title: @nullable title if null -> visibility gone
 * @param messageGravity: message gravity -> predefined left
 * @param positiveListener: @nullable  if null -> dialog.dissmiss
 * @param positiveButtonText: @nullable if null -> r.string.ok
 */
fun Activity?.showInfoDialog(message: Any, title: Any? = null, messageGravity: Int = Gravity.LEFT, makeHtml: Boolean = false,
                             positiveButtonText: Any? = null, positiveListener: ((dialogPos: IDialogInterface) -> Unit)? = null): GenericDialog? {
    if (this?.isFinishing != false) return null
        return GenericDialog.Builder(this)
            .setPositiveButton(positiveButtonText, positiveListener = positiveListener)
            .setMessage(message, makeHtml, messageGravity)
            .setTitle(title)
            .show()
}

/** Question Dialog *
 * @param message : @nonnull message
 * @param title: @nullable title if null -> visibility gone
 * @param positiveListener: @nullable  if null -> dialog.dissmiss
 * @param positiveButtonText: @nullable if null -> r.string.ok
 * @param negativeListener: @nullable  if null -> dialog.dissmiss
 * @param negativeButtonText: @nullable if null -> r.string.cancel
 */
fun Activity?.showQuestionDialog(message: Any, title: Any? = null, makeHtml: Boolean = false,
                                 positiveButtonText: Any? = null, positiveListener: ((dialogPos: IDialogInterface) -> Unit)? = null,
                                 negativeButtonText: Any? = null, negativeListener: ((dialogNeg: IDialogInterface) -> Unit)? = null): GenericDialog? {
    if (this?.isFinishing != false) return null
    return GenericDialog.Builder(this)
            .setPositiveButton(positiveButtonText, positiveListener = positiveListener)
            .setNegativeButton(negativeButtonText
                    ?: R.string.cancel, negativeListener = negativeListener)
            .setMessage(message, makeHtml)
            .setTitle(title)
            .show()
}

/** Retry Dialog *
 * @param message : @nonnull message
 * @param title: @nullable title if null -> visibility gone
 * @param positiveListener: @nullable  if null -> dialog.dissmiss
 * @param negativeListener: @nullable  if null -> dialog.dissmiss
 */
fun Activity?.showRetryDialog(message: Any, title: Any? = null,
                              positiveListener: ((dialogPos: IDialogInterface) -> Unit)?,
                              negativeListener: ((dialogNeg: IDialogInterface) -> Unit)?, imageResId: Int? = null): GenericDialog? {
    if (this?.isFinishing != false) return null
    return GenericDialog.Builder(this)
            .setPositiveButton(R.string.action_retry, positiveListener = positiveListener)
            .setNegativeButton(R.string.cancel, negativeListener = negativeListener)
            .setMessage(message)
            .setTitle(title)
            .setImage(imageResId)
            .show()
}
