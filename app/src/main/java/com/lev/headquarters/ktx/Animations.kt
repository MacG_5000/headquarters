@file:Suppress("NOTHING_TO_INLINE", "UNUSED_PARAMETER")

package com.lev.headquarters.ktx

import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.*
import androidx.core.view.*
import com.lev.headquarters.R

const val ANIMATION_DURATION_MEDIUM: Long = 400
const val ANIMATION_DURATION_LONG: Long = 800
const val ANIMATION_DURATION_SHORT: Long = 200

/**
 *  when animation end, cleared animation
 * */
inline fun Animation.clearAfter(view: View) {
    this.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) {}
        override fun onAnimationEnd(animation: Animation?) {
            view.clearAnimation()
        }

        override fun onAnimationStart(animation: Animation?) {}
    })
}

fun Animation.onAnimationEnd(predicate: (animation: Animation?) -> Unit) {
    this.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) {}
        override fun onAnimationEnd(animation: Animation?) {
            predicate.invoke(animation)
        }

        override fun onAnimationStart(animation: Animation?) {}
    })
}

inline fun View.translate(fromX: Float, toX: Float, fromY: Float, toY: Float, afterVisible: Boolean, duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = LinearInterpolator()) {
    this.animation = TranslateAnimation(
            TranslateAnimation.RELATIVE_TO_PARENT, fromX,
            TranslateAnimation.RELATIVE_TO_PARENT, toX,
            TranslateAnimation.RELATIVE_TO_PARENT, fromY,
            TranslateAnimation.RELATIVE_TO_PARENT, toY
    ).apply {
        this.fillAfter = true
        this.duration = duration
        this.zAdjustment = TranslateAnimation.ZORDER_TOP
        this.interpolator = interpolator
        this.clearAfter(this@translate)
    }
    isVisible = afterVisible
}

/**
 * Slide up view view's height from top to out
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.slideOutUp(duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = LinearInterpolator()) {
    this.translate(0f, 0f, 0f, -1f, false, duration, interpolator)
}

/**
 * Slide down view views height from out to top
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.slideInDown(duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = LinearInterpolator()) {
    this.translate(0f, 0f, -1f, 0f, true, duration, interpolator)
}

/**
 * Slide down view view's height from bottom to out
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.slideDownFromTop(duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = LinearInterpolator()) {
    this.translate(0f, 0f, 0f, 1f, false, duration, interpolator)
}

/**
 * Slide up view view's height from in to bottom
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.slideUpFromBottom(duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = LinearInterpolator()) {
    this.translate(0f, 0f, 1f, 0f, true, duration, interpolator)
}

/**
 * Collapse view via height
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.collapseHeight(duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = LinearInterpolator()) {
    measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    layoutParams.height = 0
    visibility = View.VISIBLE
    val initialHeight = measuredHeight + marginTop + marginBottom
    val anim = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            if (interpolatedTime == 1f) {
                visibility = View.GONE
            } else {
                layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                requestLayout()
            }
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }
    anim.duration = duration
    anim.clearAfter(this)
    anim.interpolator = interpolator
    startAnimation(anim)
}

/**
 * Expand view via height
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.expandHeight(duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = LinearInterpolator()) {
    measure(
            View.MeasureSpec.makeMeasureSpec((parent as View).width, View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(1024, View.MeasureSpec.AT_MOST)
    )
    val targetHeight = measuredHeight + marginTop + marginBottom
    val anim = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            visibility = View.VISIBLE
            layoutParams.height =if (interpolatedTime == 1f) ViewGroup.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
            requestLayout()
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }
    anim.interpolator = interpolator
    anim.duration = duration
    //anim.clearAfter(this)
    startAnimation(anim)
}

/**
 * Shrink animation
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.animateShrink(duration: Long = ANIMATION_DURATION_SHORT, interpolator: Interpolator = LinearInterpolator()): Animation {
    val a = (android.view.animation.AnimationUtils.loadAnimation(context, R.anim.shrink) as AnimationSet).apply {
        this.duration = duration
        this.interpolator = interpolator
    }
    startAnimation(a)
    return a
}

/**
 * Grow animation
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.animateNormalizeShrink(duration: Long = ANIMATION_DURATION_SHORT, interpolator: Interpolator = LinearInterpolator()): Animation {
    val a = (android.view.animation.AnimationUtils.loadAnimation(context, R.anim.normalize_shrink) as AnimationSet).apply {
        this.duration = duration
        this.interpolator = interpolator
    }
    startAnimation(a)
    return a
}


/**
 * Shrink animation
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.animateGrow(duration: Long = ANIMATION_DURATION_SHORT, interpolator: Interpolator = LinearInterpolator()): Animation {
    val a = (android.view.animation.AnimationUtils.loadAnimation(context, R.anim.grow) as AnimationSet).apply {
        this.duration = duration
        this.interpolator = interpolator
    }
    startAnimation(a)
    return a
}

/**
 * Grow animation
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.animateNormalizeGrow(duration: Long = ANIMATION_DURATION_SHORT, interpolator: Interpolator = LinearInterpolator()): Animation {
    val a = (android.view.animation.AnimationUtils.loadAnimation(context, R.anim.normalize_grow) as AnimationSet).apply {
        this.duration = duration
        this.interpolator = interpolator
    }
    startAnimation(a)
    return a
}

/**
 * Shrink and Grow Animation with touch complete listener
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
fun View.registerOnTouchEventWithShrinkAnimation(completeListener: (View) -> Unit) {
    setOnTouchListener { _, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                animateShrink()
                true
            }
            MotionEvent.ACTION_CANCEL -> {
                animateNormalizeShrink()
                true
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_OUTSIDE -> {
                animateNormalizeShrink()
                completeListener.invoke(this)
                true
            }
            else -> false
        }
    }
}

fun View.registerOnTouchEventWithGrowAnimation(completeListener: (View) -> Unit) {
    setOnTouchListener { _, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                animateGrow()
                true
            }
            MotionEvent.ACTION_CANCEL -> {
                animateNormalizeGrow()
                true
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_OUTSIDE -> {
                animateNormalizeGrow()
                completeListener.invoke(this)
                true
            }
            else -> false
        }
    }
}

/**
 * Expand view via Width
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.expandWidth(duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = LinearInterpolator()) {
    measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    val targetWidth = measuredWidth
    val anim = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            layoutParams.width = if (interpolatedTime == 1f) ViewGroup.LayoutParams.WRAP_CONTENT else (targetWidth * interpolatedTime).toInt()
            requestLayout()
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }
    anim.duration = duration
    anim.clearAfter(this)
    anim.interpolator = interpolator
    startAnimation(anim)
}


/**
 * Collapse view via Width
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.collapseWidth(duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = AccelerateInterpolator()): Animation {
    val initialWidth = measuredWidth + marginLeft + marginRight
    val anim = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            if (interpolatedTime == 1f) {
                visibility = View.GONE
            } else {
                layoutParams.width = initialWidth - (initialWidth * interpolatedTime).toInt()
                requestLayout()
            }
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }
    anim.duration = duration
    anim.clearAfter(this)
    anim.interpolator = interpolator
    startAnimation(anim)
    return anim
}

/**
 * Collapse view via Width
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.fadeIn(duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = LinearInterpolator()) {
    startAnimation(AlphaAnimation(0f, 1f).apply {
        this.duration = duration
        this.fillAfter = true
        this.interpolator = interpolator
        this.clearAfter(this@fadeIn)
    })
}

/**
 * Collapse view via Width
 * @param duration How long this animation should last. The duration cannot be negative.
 * @param interpolator -> Sets the acceleration curve for this animation. Defaults to a linear interpolation.
 * @throws java.lang.IllegalArgumentException if the duration is < 0
 * */
inline fun View.fadeOut(duration: Long = ANIMATION_DURATION_MEDIUM, interpolator: Interpolator = LinearInterpolator()) {
    startAnimation(AlphaAnimation(1f, 0f).apply {
        this.duration = duration
        this.fillAfter = true
        this.interpolator = interpolator
        this.clearAfter(this@fadeOut)
    })
}

/**
 * Shake animation
 * */
inline fun View.shakeIt() {
    startAnimation((android.view.animation.AnimationUtils.loadAnimation(context, R.anim.shake) as AnimationSet).apply {
        clearAfter(this@shakeIt)
    })
}

inline fun View.hopIt() {
    startAnimation((android.view.animation.AnimationUtils.loadAnimation(context, R.anim.hop) as AnimationSet).apply {
        clearAfter(this@hopIt)
    })
}

/**
 * slidng view horizontaly
 * @param value pixel size sliding -x +x
 * */
inline fun View.slideViewHorizontal(value: Int) {
    val distance = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value.toFloat(), resources.displayMetrics)
    animate().translationX(distance).setDuration(ANIMATION_DURATION_MEDIUM).start()
}

inline fun View.overshootAnimation() {
    startAnimation((android.view.animation.AnimationUtils.loadAnimation(context, R.anim.overshoot) as AnimationSet).apply {
        clearAfter(this@overshootAnimation)
    })
}

inline fun View.animateIncrease(duration: Long = ANIMATION_DURATION_SHORT, interpolator: Interpolator = LinearInterpolator()) {
    animateGrow().onAnimationEnd { this.animateNormalizeGrow()}

}

inline fun View.animateDecrease(duration: Long = ANIMATION_DURATION_SHORT, interpolator: Interpolator = LinearInterpolator()) {
    animateShrink().onAnimationEnd { this.animateNormalizeShrink()}
}

inline fun View.animateZoomIn(duration: Long = ANIMATION_DURATION_SHORT, interpolator: Interpolator = AccelerateInterpolator(), fromX:Float = 0f, fromY:Float = 0f){
    startAnimation(ScaleAnimation(fromX, 1.0f, fromY, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f).apply {
        this.duration = duration
        this.startOffset = 300
        this.interpolator = interpolator
        this.clearAfter(this@animateZoomIn)
    })
}

inline fun View.animateZoomOut(duration: Long = ANIMATION_DURATION_SHORT, interpolator: Interpolator = AccelerateInterpolator()){
    startAnimation(ScaleAnimation(1.0f, 0f, 1.0f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f).apply {
        this.duration = duration
        this.startOffset = 500
        this.interpolator = interpolator
        this.clearAfter(this@animateZoomOut)
    })
}



