@file:Suppress("NOTHING_TO_INLINE")

package com.lev.headquarters.ktx

import androidx.annotation.IntRange
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

const val NONE_DECIMAL_PATTERN: String = "###,##0"
const val ONE_DECIMAL_PATTERN: String = "###,##0.0"
const val TWO_DECIMAL_PATTERN: String = "###,##0.00"


inline fun Double.asString(@IntRange(from = 0, to = 2) decimalCount: Int = 2, local: Locale = Locale.getDefault()): String = when (decimalCount) {
    0 -> this.asStringViaPattern(NONE_DECIMAL_PATTERN, local)
    1 -> this.asStringViaPattern(ONE_DECIMAL_PATTERN, local)
    else -> this.asStringViaPattern(TWO_DECIMAL_PATTERN, local)
}

inline fun Double.asStringViaPattern(decimalPattern: String, local: Locale = Locale.getDefault()): String {
    val decimalFormat = NumberFormat.getNumberInstance(local) as DecimalFormat
    decimalFormat.applyPattern(decimalPattern)
    return decimalFormat.format(this)
}