package com.lev.headquarters.ktx

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import androidx.annotation.ColorRes
import java.util.*

//fun String.formatPhoneNumber(defaultCountryIso: String = "TR"): String =
  //  PhoneNumberUtils.formatNumber(this, defaultCountryIso)
    //if (isOverApi(Build.VERSION_CODES.LOLLIPOP)) {
     //   PhoneNumberUtils.formatNumber(this, defaultCountryIso)
    //} else this

/*
@Suppress("DEPRECATION")
fun String.makeHtml(): Spanned = if (isOverApi(Build.VERSION_CODES.N)) {
    Html.fromHtml(this, Html.FROM_HTML_MODE_COMPACT)
} else Html.fromHtml(this)

*/

/**
 * Makes a substring of a string bold.
 * @reciever-> Full text
 * @param boldText Text you want to make bold
 * @local language local -> default
 * @return String with bold substring
 */
fun String.makeBoldSection(boldText: String, locale: Locale = Locale.getDefault()): SpannableStringBuilder {
    val builder = SpannableStringBuilder()
    if (boldText.isNotEmpty() && boldText.trim { it <= ' ' } != "") {
        //for counting start/end indexes
        val testText = this.toLowerCase(locale)
        val testTextToBold = boldText.toLowerCase(locale)
        val startingIndex = testText.indexOf(testTextToBold)
        val endingIndex = startingIndex + testTextToBold.length
        //for counting start/end indexes
        if (startingIndex < 0 || endingIndex < 0) {
            return builder.append(this)
        } else if (startingIndex >= 0 && endingIndex >= 0) {
            builder.append(this)
            builder.setSpan(StyleSpan(Typeface.BOLD), startingIndex, endingIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    } else {
        return builder.append(this)
    }
    return builder
}

/**
 * Makes a substring of a string bold.
 * @reciever-> Full text
 * @param colorText Text you want to make colered
 * @param colorResId Text you want to make which color
 * @local language local -> default
 * @return String with colered substring
 */
fun String.makeColorSection(context: Context, colorText: String?,
                            @ColorRes colorResId: Int,
                            locale: Locale = Locale.getDefault()): SpannableStringBuilder {
    val builder = SpannableStringBuilder()
    if (colorText?.isNotEmpty() == true && colorText.trim { it <= ' ' } != "") {
        //for counting start/end indexes
        val testText = this.toLowerCase(locale)
        val testTextToColor = colorText.toLowerCase(locale)
        val startingIndex = testText.indexOf(testTextToColor)
        val endingIndex = startingIndex + testTextToColor.length
        //for counting start/end indexes
        if (startingIndex < 0 || endingIndex < 0) {
            return builder.append(this)
        } else if (startingIndex >= 0 && endingIndex >= 0) {
            builder.append(this)
            builder.setSpan(
                ForegroundColorSpan(context.color(colorResId)),
                startingIndex,
                endingIndex,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    } else {
        return builder.append(this)
    }
    return builder
}

fun String.isPhone(): Boolean {
    val p = "^1([34578])\\d{9}\$".toRegex()
    return matches(p)
}

fun String.isNumeric(): Boolean {
    val p = "^[0-9]+$".toRegex()
    return matches(p)
}

fun String.isEmail(): Boolean {
    val p = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$".toRegex()
    return matches(p)
}

fun <E> ArrayList<E>.toStringWithout(): String {
    val stringList = this.toString()
    return stringList.substring(1, stringList.lastIndex)
}

fun String.toUpperFirstChar(): String {
    if(isNullOrBlank()) return ""
    return this.substring(0, 1).toUpperCase() + this.substring(1).toLowerCase()
}

fun String.containsNumbers(): Boolean {
    val p = "(.)*(\\d)(.)*".toRegex()
    return matches(p)
}

fun String.containsNonAlphanumeric(): Boolean {
    val pattern = "^.*[^/\\p{L}/u' ].*\$".toRegex()//L bütün dilleri temsil eder, ' ve boşluk bunlar haricindeki alphanumeric karakterleri temsil eder
    // , p:property,L:languages,u:unicode
    return matches(pattern)
}
