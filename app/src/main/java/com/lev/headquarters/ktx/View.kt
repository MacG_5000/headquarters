@file:Suppress("NOTHING_TO_INLINE")

package com.lev.headquarters.ktx

import android.app.Activity
import android.content.Context
import android.graphics.Paint
import android.graphics.Rect
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.widget.*
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager

fun ViewGroup.findFirstScrollViews(): View? {
    var view: View?
    for (i in 0..this.childCount) {
        view = this.getChildAt(i) ?: continue
        if (!view.isVisible) continue
        if (view.isScrollableView) return view
        if (view is ViewGroup) {
            view = view.findFirstScrollViews() ?: continue
            return view
        }
    }
    return null
}

inline val View.isScrollableView: Boolean
    get() = this is ScrollView ||
            this is HorizontalScrollView ||
            this is NestedScrollView ||
            this is AbsListView ||
            this is RecyclerView ||
            this is ViewPager ||
            this is WebView

inline fun View.contains(x: Float, y: Float): Boolean {
    val localRect = Rect()
    this.getGlobalVisibleRect(localRect)
    return localRect.contains(x.toInt(), y.toInt())
}

inline fun View?.canViewScrollUp(x: Float, y: Float, default: Boolean = false): Boolean {
    if (this?.contains(x, y) != true)
        return default
    return this.canScrollVertically(-1)
}

inline fun View?.canViewScrollDown(x: Float, y: Float, default: Boolean = false): Boolean {
    if (this?.contains(x, y) != true)
        return default
    return this.canScrollVertically(1)
}

inline fun View?.canViewScrollLeft(x: Float, y: Float, default: Boolean = false): Boolean {
    if (this?.contains(x, y) != true)
        return default
    return this.canScrollHorizontally(1)
}

inline fun View?.canViewScrollRight(x: Float, y: Float, default: Boolean = false): Boolean {
    if (this?.contains(x, y) != true)
        return default
    return this.canScrollHorizontally(-1)
}

/**EditText*/
inline fun EditText.asString(): String {
    return text.toString()
}

inline var View.asEnable: Boolean
    get() = isEnabled
    set(value) {
        isEnabled = value
        isClickable = value
        isFocusableInTouchMode = value
        isFocusable = value
    }

fun TextView.underLine() {
    paint.flags = paint.flags or Paint.UNDERLINE_TEXT_FLAG
    paint.isAntiAlias = true
}

fun EditText.onActionDone(onDone: () -> Unit) {
    imeOptions = EditorInfo.IME_ACTION_DONE
    setSingleLine(true)
    maxLines = 1
    setOnEditorActionListener(TextView.OnEditorActionListener { _, keyCode, _ ->
        if (keyCode == EditorInfo.IME_ACTION_DONE) {
            onDone()
            return@OnEditorActionListener true
        }
        false
    })
}


inline fun View.setMargins(left: Int, top: Int, right: Int, bottom: Int) {
    val params: ViewGroup.MarginLayoutParams = layoutParams as ViewGroup.MarginLayoutParams
    params.setMargins(left, top, right, bottom)
    layoutParams = params
}


    /** EditText [CompondDrawablesIndex]'a göre click event verir.
    etName.setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.mipmap.ic_form_time, null),null)
    etName.onDrawableClick(CompondDrawablesIndex.DRAWABLE_RIGHT, onClick = {
    Log.i("", "asdad")
    })*/
fun EditText.onDrawableRightClick(drawable: Int, onClick: () -> Unit) {
    setOnTouchListener(View.OnTouchListener { _, event ->
        if (compoundDrawables.isEmpty() || compoundDrawables[drawable] == null) return@OnTouchListener false
        if (event.action == MotionEvent.ACTION_UP) {
            if (event.rawX >= right - (compoundDrawables[2].bounds.width() + paddingRight)) {
                onClick.invoke()
                return@OnTouchListener true
            }
        }
        false
    })
}

fun EditText.onTextChanged(listener: (s: CharSequence?, start: Int, before: Int, count: Int) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            listener.invoke(s, start, before, count)
        }
    })
}

fun View.onTouchHideKeyboard(activity: Activity?) {
    setOnTouchListener { _, event ->
        if (event.action == MotionEvent.ACTION_DOWN || event.action == MotionEvent.ACTION_MOVE) {
            activity?.hideSoftKeyboard()
            return@setOnTouchListener false
        }
        return@setOnTouchListener false
    }
}

fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, 0)
}

fun View.hideKeyboard(): Boolean {
    try {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

fun View.keyBoardState(): Boolean {
    return (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).isAcceptingText
}
/*

fun View.addRipple() = with(TypedValue()) {
    isClickable = true
    isFocusable = true
    context.theme.resolveAttribute(android.R.attr.selectableItemBackground, this, true)
    setBackgroundResource(resourceId)
}


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun View.addCircleRipple() = with(TypedValue()) {
    isClickable = true
    isFocusable = true
    context.theme.resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, this, true)
    setBackgroundResource(resourceId)
}


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun View.addScrollOutline(scrollView: View) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this.stateListAnimator = AnimatorInflater.loadStateListAnimator(context, R.animator.animator_toolbar_elevation)
        scrollView.setOnScrollChangeListener { _, _, _, _, _ ->
            this.isSelected = scrollView.canScrollVertically(-1)
        }
    } else {
        elevation = context.resources.getDimension(R.dimen.z_6)
    }
}
*/



