package com.lev.headquarters.ktx

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.RequiresPermission
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.lev.headquarters.R

@RequiresPermission(android.Manifest.permission.ACCESS_NETWORK_STATE)
fun Activity?.isOnline(view: View?, predicate: () -> Unit) {
    if (this?.isFinishing != false || view == null) return
    val netInfo =
            (this.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
    if (netInfo != null && netInfo.isConnected) {
        predicate.invoke()
    } else {
        showRetrySnackBar(view, predicate)
    }
}

@RequiresPermission(android.Manifest.permission.ACCESS_NETWORK_STATE)
fun Activity.showRetrySnackBar(view: View?, predicate: () -> Unit) {
    view?.let {
        Snackbar
                .make(view, R.string.error_connection, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry) {
                    isOnline(view, predicate)
                }
                .setActionTextColor(color(R.color.colorAccent))
                .show()
    } ?: return
}

fun Activity?.showSnackMessage(view: View?, message: Any, @ColorRes backgroundColorRes: Int = R.color.colorAccent, @DrawableRes iconRes: Int? = null) {
    if (this?.isFinishing != false || view == null) return
    val snack = Snackbar.make(view, asString(message) ?: "", Snackbar.LENGTH_LONG)
    val snackView = snack.view
    snackView.setBackgroundColor(color(backgroundColorRes))
    iconRes?.let {
        val tvSnack = snackView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        tvSnack.setCompoundDrawablesWithIntrinsicBounds(it, 0, 0, 0)
        tvSnack.compoundDrawablePadding = resources.getDimensionPixelOffset(R.dimen.spacing_10)
    }
    snack.show()
}

fun Activity.hideSoftKeyboard() {
    if (currentFocus != null) {
        val inputMethodManager = getSystemService(Context
                .INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    }
}

