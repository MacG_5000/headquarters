package com.lev.headquarters.data.api.dataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.lev.headquarters.common.Result
import com.lev.headquarters.common.throwable.BaseThrowable
import com.lev.headquarters.common.throwable.ListEmptyThrowable
import com.lev.headquarters.data.api.MovieService
import com.lev.headquarters.data.api.entity.Movie
import com.lev.headquarters.ktx.DATE_FORMAT17
import com.lev.headquarters.ktx.DATE_FORMAT18
import com.lev.headquarters.ktx.changeDateFormat
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class UpcomingDataSource@Inject constructor(private val service: MovieService,
                                            private val disposable: CompositeDisposable,
                                            private val state: MutableLiveData<Result<Boolean>>
) : PageKeyedDataSource<Int, Movie>() {

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Movie>) {

        disposable.add(
            service.getUpcoming()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap {
                    if (it.total_results == 0 || it.results.isEmpty())
                        Single.error(ListEmptyThrowable())
                    else Single.just(it.apply {
                        results.forEach { m -> m.releaseDate = m.releaseDate.changeDateFormat(DATE_FORMAT17, DATE_FORMAT18)?:"" }
                    })
                }
                .subscribe(
                    {
                        callback.onResult(it.results, null, if (it.total_pages > 1) 2 else null)
                    },
                    {
                        if (it is BaseThrowable)
                            state.value = Result.error(it)
                        else
                            state.value = Result.error(BaseThrowable(it))
                        //todo crashlytics
                    })
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
        disposable.add(
            service.getUpcoming(params.key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap {
                    if (it.total_results == 0 || it.results.isEmpty())
                        Single.error(ListEmptyThrowable())
                    else Single.just(it.apply {
                        results.forEach { m -> m.releaseDate = m.releaseDate.changeDateFormat(DATE_FORMAT17, DATE_FORMAT18)?:"" }
                    })
                }
                .subscribe(
                    {
                        callback.onResult(it.results, if (it.total_pages == params.key) null else (params.key + 1))
                    },
                    {
                        if (it is BaseThrowable)
                            state.value = Result.error(it)
                        else
                            state.value = Result.error(BaseThrowable(it))
                        //todo crashlytics
                    })
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {}

}