package com.lev.headquarters.data.api

import com.lev.headquarters.constants.ApiQuery
import com.lev.headquarters.constants.ApiUrls
import com.lev.headquarters.data.api.entity.SearchResponse
import com.lev.headquarters.data.api.entity.response.MovieDetailResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    @GET(ApiUrls.SEARCH)
    fun search(@Query(ApiQuery.QUERY) query: String, @Query(ApiQuery.PAGE) page: Int = 1): Single<SearchResponse>

    @GET(ApiUrls.MOVIE)
    fun getDetail(@Path(ApiQuery.ID) id: Int): Single<MovieDetailResponse>

    @GET(ApiUrls.TOP_RATED)
    fun getTopRated(@Query(ApiQuery.PAGE) page: Int = 1): Single<SearchResponse>

    @GET(ApiUrls.UPCOMING)
    fun getUpcoming(@Query(ApiQuery.PAGE) page: Int = 1): Single<SearchResponse>

    @GET(ApiUrls.NOW_PLAYIN)
    fun getNowPlaying(@Query(ApiQuery.PAGE) page: Int = 1): Single<SearchResponse>

}