package com.lev.headquarters.data.api.entity.response

data class Country(val iso_3166_1: String,
                   val name: String)