package com.lev.headquarters.data.api.entity.response

data class Genre(val id: Int,
                 val name: String)