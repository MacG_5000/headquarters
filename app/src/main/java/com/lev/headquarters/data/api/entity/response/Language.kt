package com.lev.headquarters.data.api.entity.response

data class Language(val iso_639_1: String,
                    val name: String)