package com.lev.headquarters.data.api.dataSource

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.lev.headquarters.common.Result
import com.lev.headquarters.common.throwable.BaseThrowable
import com.lev.headquarters.common.throwable.ListEmptyThrowable
import com.lev.headquarters.data.api.MovieService
import com.lev.headquarters.data.api.entity.Movie
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MoviesDataSource @Inject constructor(private val query: String,
                                           private val service: MovieService,
                                           private val disposable: CompositeDisposable,
                                           private val state: MutableLiveData<Result<Boolean>>
) : PageKeyedDataSource<Int, Movie>() {

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Movie>) {

        disposable.add(
            service.search(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap {
                    if (it.total_results == 0 || it.results.isEmpty())
                        Single.error(ListEmptyThrowable())
                    else Single.just(it)
                }
                .subscribe(
                    {
                        callback.onResult(it.results, null, if (it.total_pages > 1) 2 else null)
                    },
                    {
                        Log.d("asd", it.message)
                        if(it is BaseThrowable)
                            state.value = Result.error(it)
                        else
                            state.value = Result.error(BaseThrowable(it))
                        //todo crashlytics
                    })
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
        disposable.add(
            service.search(query, params.key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap {
                    if (it.total_results == 0 || it.results.isEmpty())
                        Single.error(ListEmptyThrowable())
                    else Single.just(it)
                }
                .subscribe(
                    {
                        callback.onResult(it.results, if (it.total_pages == params.key) null else (params.key + 1))
                    },
                    {
                        if(it is BaseThrowable)
                            state.value = Result.error(it)
                        else
                            state.value = Result.error(BaseThrowable(it))
                        //todo crashlytics
                    })
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {}

}