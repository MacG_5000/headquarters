package com.lev.headquarters.data.api.dataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.lev.headquarters.common.Result
import com.lev.headquarters.constants.DataSourceType
import com.lev.headquarters.data.api.MovieService
import com.lev.headquarters.data.api.entity.Movie
import io.reactivex.disposables.CompositeDisposable

class MoviesDataSourceFactory(private val dataSourceType: Int,
                              private val query: String,
                              private val compositeDisposable: CompositeDisposable,
                              private val state: MutableLiveData<Result<Boolean>>,
                              private val webService: MovieService)
    : DataSource.Factory<Int, Movie>() {

    override fun create(): DataSource<Int, Movie> {
        if (query.isNotEmpty())
            return MoviesSearchDataSource(query, webService, compositeDisposable, state)
        when (dataSourceType){
            DataSourceType.UPCOMING -> return UpcomingDataSource(webService, compositeDisposable, state)
            DataSourceType.TOP_RATED -> return TopRatedDataSource(webService, compositeDisposable, state)
            DataSourceType.NOW_PLAYING -> return NowPlayingDataSource(webService, compositeDisposable, state)
        }
        return MoviesSearchDataSource(query, webService, compositeDisposable, state)
        /*return if (query.isNotEmpty())
            MoviesSearchDataSource(query, webService, compositeDisposable, state)
        else
            UpcomingDataSource(webService, compositeDisposable, state)*/
    }
}