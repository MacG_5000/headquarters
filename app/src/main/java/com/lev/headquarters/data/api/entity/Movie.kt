package com.lev.headquarters.data.api.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(val id: Int,
                 @SerializedName("vote_count") val voteCount: Int,
                 @SerializedName("vote_average") val voteAverage: Double,
                 @SerializedName("poster_path") val posterPath: String,
                 @SerializedName("backdrop_path") val backdropPath: String,
                 @SerializedName("original_language") val originalLanguage: String,
                 @SerializedName("original_title") val originalTitle: String,
                 @SerializedName("release_date") var releaseDate: String,
                 val overview: String,
                 val title: String) : Parcelable