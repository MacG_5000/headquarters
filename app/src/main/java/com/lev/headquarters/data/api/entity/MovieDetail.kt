package com.lev.headquarters.data.api.entity

import com.lev.headquarters.data.api.entity.response.Country
import com.lev.headquarters.data.api.entity.response.Genre
import com.lev.headquarters.data.api.entity.response.Language
import com.lev.headquarters.data.api.entity.response.MovieDetailResponse
import com.lev.headquarters.ktx.toUpperFirstChar

class MovieDetail(val budget: Int,
                  val genres: String,
                  val id: Int,
                  val originalLanguage: String,
                  val originalTitle: String,
                  val overview: String,
                  val countries: String,
                  val languages: String,
                  val revenue: Int) {

    constructor(r: MovieDetailResponse) : this(
        r.budget,
        getGenres(r.genres),
        r.id,
        r.original_language,
        r.original_title,
        r.overview,
        getCountries(r.production_countries),
        getLanguages(r.spoken_languages),
        r.revenue)

    companion object {
        private fun getGenres(genres: List<Genre>): String {
            var s = ""

            genres.forEachIndexed { index, genre ->
                s += genre.name
                if (index != genres.lastIndex) {
                    s += ", "
                }
            }
            return s
        }

        private fun getLanguages(languages: List<Language>): String {
            var s = ""

            languages.forEachIndexed { index, l ->
                s += l.iso_639_1.toUpperFirstChar()
                if (index != languages.lastIndex) {
                    s += ", "
                }
            }
            return s
        }

        private fun getCountries(countries: List<Country>): String {
            var s = ""

            countries.forEachIndexed { index, l ->
                s += l.iso_3166_1.toUpperFirstChar()
                if (index != countries.lastIndex) {
                    s += ", "
                }
            }
            return s
        }
    }
}