package com.lev.headquarters.data.api.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchResponse(val page: Long = 1,
                          val total_results: Int = 0,
                          val total_pages: Int = 0,
                          val results: List<Movie> = emptyList()): Parcelable