package com.lev.headquarters.data.api.entity.response

data class MovieDetailResponse(val budget: Int,
                               val genres: List<Genre>,
                               val id: Int,
                               val original_language: String,
                               val original_title: String,
                               val overview: String,
                               val production_countries: List<Country>,
                               val spoken_languages: List<Language>,
                               val revenue: Int)