package com.lev.headquarters.modules.splash

import android.content.Intent
import android.os.Bundle
import com.lev.headquarters.R
import com.lev.headquarters.base.BaseActivity
import com.lev.headquarters.modules.main.MainActivity
import java.util.*
import kotlin.concurrent.timerTask

class SplashActivity : BaseActivity(R.layout.activity_splash){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val task = timerTask {
            finish()
            startActivity(Intent(applicationContext, MainActivity::class.java))
        }
        val timer = Timer()
        timer.schedule(task, 2000)
    }
}