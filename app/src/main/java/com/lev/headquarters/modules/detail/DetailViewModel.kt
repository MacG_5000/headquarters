package com.lev.headquarters.modules.detail

import androidx.lifecycle.MutableLiveData
import com.lev.headquarters.base.viewmodel.BaseViewModel
import com.lev.headquarters.common.Result
import com.lev.headquarters.common.throwable.BaseThrowable
import com.lev.headquarters.data.api.MovieService
import com.lev.headquarters.data.api.entity.Movie
import com.lev.headquarters.data.api.entity.MovieDetail
import com.lev.headquarters.ktx.handle
import com.lev.headquarters.ktx.toResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailViewModel @Inject constructor(private val movieService: MovieService): BaseViewModel(){

    var movie: Movie? = null

    val liveDetail: MutableLiveData<Result<MovieDetail>> = MutableLiveData()


    fun getDetail(){
        movie?.let {m->
            subscribe(movieService.getDetail(m.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    MovieDetail(it)
                }
                .toResult()
                .handle(liveDetail)
            )

        }?:kotlin.run {
            liveDetail.value = Result.error(BaseThrowable(finishScreen = false))
        }
    }

}