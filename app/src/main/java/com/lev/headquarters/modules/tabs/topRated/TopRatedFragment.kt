package com.lev.headquarters.modules.tabs.topRated

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.lev.headquarters.R
import com.lev.headquarters.base.BaseFragment
import com.lev.headquarters.base.viewmodel.ViewModelFactory
import com.lev.headquarters.common.SearchPagedListAdapter
import com.lev.headquarters.common.throwable.BaseThrowable
import com.lev.headquarters.common.throwable.ListEmptyThrowable
import com.lev.headquarters.data.api.entity.Movie
import com.lev.headquarters.ktx.asString
import com.lev.headquarters.ktx.isOnline
import kotlinx.android.synthetic.main.fragment_top_rated.*
import javax.inject.Inject

class TopRatedFragment: BaseFragment(R.layout.fragment_tab) {

    lateinit var viewModel: TopRatedViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var adapter: SearchPagedListAdapter

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView?
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                activity.isOnline(view) { viewModel.search(newText.orEmpty(), viewLifecycleOwner) }
                return false
            }
        })
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TopRatedViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter.apply { listener = { movie, iv -> launchDetail(movie) } }
        viewModel.liveState.observe(viewLifecycleOwner, Observer {
            if (handleState(it) == true) {
                observeData()
            }
        })
        viewModel.observePublisher()
        observeData()
    }

    override fun onError(t: BaseThrowable) {
        when (t) {
            is ListEmptyThrowable -> onListChanged(true, requireContext().asString(t.uiMessage))
            else -> super.onError(t)
        }
    }

    private fun onListChanged(isEmpty: Boolean, emptyMessage: String? = null) {
        emptyMessage?.let { tvEmpty.text = it }
        recyclerView.isVisible = !isEmpty
        flEmpty.isVisible = isEmpty
    }

    private fun observeData() {
        viewModel.movieList.observe(viewLifecycleOwner, Observer {
            onListChanged(false)
            adapter.submitList(it)
        })
    }

    private fun launchDetail(movie: Movie) {
        findNavController().navigate(
            TopRatedFragmentDirections.actionTopRatedToDetailFragment().setMovie(movie))
    }
}