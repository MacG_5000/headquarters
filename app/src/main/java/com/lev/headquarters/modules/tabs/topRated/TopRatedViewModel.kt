package com.lev.headquarters.modules.tabs.topRated

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.jakewharton.rxrelay2.PublishRelay
import com.lev.headquarters.base.viewmodel.BaseViewModel
import com.lev.headquarters.common.Result
import com.lev.headquarters.constants.Constants
import com.lev.headquarters.constants.DataSourceType
import com.lev.headquarters.data.api.MovieService
import com.lev.headquarters.data.api.dataSource.MoviesDataSourceFactory
import com.lev.headquarters.data.api.dataSource.MoviesSearchDataSource
import com.lev.headquarters.data.api.dataSource.NowPlayingDataSource
import com.lev.headquarters.data.api.entity.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TopRatedViewModel @Inject constructor(private val movieService: MovieService) : BaseViewModel() {

    companion object {
        val config
            get() = PagedList.Config.Builder()
                .setPageSize(Constants.PAGE_SIZE)
                .setPrefetchDistance(Constants.PREFETCH_DISTANCE)
                .setEnablePlaceholders(false)
                .build()
    }

    private var _movieList: LiveData<PagedList<Movie>>
    val movieList
        get() = _movieList

    private val publisher: PublishRelay<String> = PublishRelay.create()

    val liveState: MutableLiveData<Result<Boolean>> = MutableLiveData()

    init {
        _movieList = LivePagedListBuilder(MoviesDataSourceFactory(DataSourceType.TOP_RATED, "", disposables, liveState, movieService), config).build()
    }

    fun search(s: String, lifecycleOwner: LifecycleOwner) {
        if (s.length in 1..2) return
        val dataSource = _movieList.value?.dataSource
        if( dataSource == null && s.isEmpty())
            _movieList = LivePagedListBuilder(MoviesDataSourceFactory(DataSourceType.TOP_RATED,"", disposables, liveState, movieService), config).build()
        else if ((s.isEmpty() && dataSource is NowPlayingDataSource) || (dataSource is MoviesSearchDataSource && dataSource.query == s )) return
        _movieList.removeObservers(lifecycleOwner)
        publisher.accept(s)
    }

    fun observePublisher(){
        subscribe(publisher
            .debounce(300, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                _movieList = LivePagedListBuilder(MoviesDataSourceFactory(DataSourceType.TOP_RATED, it, disposables,liveState, movieService), config).build()
                liveState.value = Result.success(true)
            }
        )
    }
}