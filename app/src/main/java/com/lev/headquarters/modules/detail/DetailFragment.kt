package com.lev.headquarters.modules.detail

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.lev.headquarters.R
import com.lev.headquarters.base.BaseFragment
import com.lev.headquarters.base.viewmodel.ViewModelFactory
import com.lev.headquarters.common.LoadingIndicator
import com.lev.headquarters.common.SecurePreferencesManager
import com.lev.headquarters.constants.Constants
import com.lev.headquarters.ktx.isOnline
import com.lev.headquarters.ui.loadWithGlide
import kotlinx.android.synthetic.main.fragment_detail_end.ivBackdrop
import kotlinx.android.synthetic.main.fragment_detail_start.*
import kotlinx.android.synthetic.main.layout_movie_detail.*
import javax.inject.Inject

class DetailFragment: BaseFragment(R.layout.fragment_detail_start){

    lateinit var detailVM: DetailViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var securePreferencesManager: SecurePreferencesManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
        detailVM = ViewModelProviders.of(this, viewModelFactory).get(DetailViewModel::class.java)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            activity?.onBackPressed()
            return true
        }
        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailVM.movie = arguments?.let{ DetailFragmentArgs.fromBundle(it).movie}
        if (savedInstanceState == null || detailVM.liveDetail.value == null) {
            activity?.isOnline(view){ detailVM.getDetail() }
        }
        vLike.setOnClickListener {
            detailVM.movie?.let {movie ->
                if(securePreferencesManager.isFavorite(movie.id)){
                    ivFav.setImageResource(R.drawable.ic_like_dark)
                    detailVM.movie?.id?.let { it1 -> securePreferencesManager.removeFromFavorites(it1) }
                } else{
                    detailVM.movie?.id?.let { it1 -> securePreferencesManager.addToFavorites(it1) }
                    ivFav.setImageResource(R.drawable.ic_like_dark_active)
                }
            }
        }
        detailVM.movie?.also {
            ivBackdrop.loadWithGlide(Constants.IMAGE_BASE_URL_LARGE + it.backdropPath)
            ivPoster.loadWithGlide(Constants.IMAGE_BASE_URL_SMALL + it.posterPath)
            tvTitle.text = it.title
            tvReleaseDate.text = it.releaseDate
            tvOverview.text = it.overview
            tvScore.isVisible = it.voteAverage != 0.0
            tvScore.text = it.voteAverage.toString()
            tvOriginalLanguage.text = resources.getString(R.string.original_language, it.originalLanguage)
            if(securePreferencesManager.isFavorite(it.id))
                ivFav.setImageResource(R.drawable.ic_like_dark_active)
            else
                ivFav.setImageResource(R.drawable.ic_like_dark)
        }
        detailVM.liveDetail.observe(viewLifecycleOwner, Observer {
            handleState(it)?.let { detail->
                tvProductionCountry.text = resources.getString(R.string.p_countries, detail.countries)
                tvBudget.text = resources.getString(R.string.budget, detail.budget.toString())
                tvLanguages.text = resources.getString(R.string.languages, detail.languages)
                tvGenres.text = detail.genres
                tvSalary.text = resources.getString(R.string.revenue, detail.revenue.toString())
            }
        })
    }

    override fun setLoadingIndicator(loadingIndicator: LoadingIndicator) {
        //maybe use shimmer effect
    }
}